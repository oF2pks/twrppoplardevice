#
# Copyright 2017 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Release name
PRODUCT_RELEASE_NAME := poplar

$(call inherit-product, build/target/product/embedded.mk)

# Inherit from our custom product configuration
$(call inherit-product, vendor/omni/config/common.mk)
### BOOTANIMATION
TARGET_SCREEN_HEIGHT := 1920
TARGET_SCREEN_WIDTH := 1080
TARGET_BOOTANIMATION_HALF_RES := true
### PLATFORM
#$(call inherit-product, device/sony/poplar/platform.mk)
### PROPRIETARY VENDOR FILES
#$(call inherit-product, vendor/sony/poplar/poplar-vendor.mk)

### DALVIK/HWUI
#$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)
#$(call inherit-product, frameworks/native/build/phone-xxhdpi-2048-hwui-memory.mk)
# Time Zone data for recovery
PRODUCT_COPY_FILES += \
    system/timezone/output_data/iana/tzdata:recovery/root/system/usr/share/zoneinfo/tzdata
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    persist.sys.usb.config=mtp,adb \
    persist.service.adb.enable=1 \
    persist.service.debuggable=1 \
    ro.adb.secure=0 \
    ro.debuggable=1 \
    ro.allow.mock.location=1 \
    ro.secure=0
#    ro.display.series=OnePlus 5
# TWRP    $(LOCAL_PATH)/recovery/postrecoveryboot.sh:/root/sbin/postrecoveryboot.sh \
#PRODUCT_COPY_FILES += \
#    $(PLATFORM_PATH)/recovery/init.recovery.poplar.rc:recovery/root/init.recovery.poplar.rc \
#    $(PLATFORM_PATH)/recovery/init.recovery.usb.rc:recovery/root/init.recovery.usb.rc \
#    $(PLATFORM_PATH)/recovery/fstab.poplar:recovery/root/fstab.poplar
#Add manifest for hwservicemanager
#PRODUCT_COPY_FILES += \
#    $(PLATFORM_PATH)/recovery/vendor/manifest.xml:recovery/root/vendor/manifest.xml


#DEVICE_PATH := device/sony/poplar

#include $(DEVICE_PATH)/device/*.mk

#include $(DEVICE_PATH)/device/init.mk



## Device identifier. This must come after all inclusions
PRODUCT_NAME := omni_poplar
PRODUCT_DEVICE := poplar
PRODUCT_BRAND := Sony
PRODUCT_MODEL := G8341
PRODUCT_MANUFACTURER := Sony
