#!/sbin/sh


mkdir /s
mount -t ext4 -o ro /dev/block/bootdevice/by-name/system /s

if [ -f /s/build.prop ]; then
	osver=$(grep -i 'ro.build.version.release' /s/build.prop  | cut -f2 -d'=')
	patchlevel=$(grep -i 'ro.build.version.security_patch' /s/build.prop  | cut -f2 -d'=')
	setprop ro.build.version.release $osver
	setprop ro.build.version.security_patch "$patchlevel"
fi
    umount /s

    setprop crypto.ready 1

    exit 0
