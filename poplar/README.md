# android_device_poplar
Tree for building TWRP for poplar

## Sources

  `<project name="cryptomilk/android_kernel_sony_msm8998" path="kernel/sony/msm8998" remote="github" revision="lineage-15.1"/>`


## To compile
(low ram PCs: sudo mount -o remount,size=4G,noatime /tmp)
export ALLOW_MISSING_DEPENDENCIES=true

. build/envsetup.sh && lunch omni_poplar-eng

mka adbd recoveryimage

